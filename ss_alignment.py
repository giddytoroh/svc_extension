import sys
audio_path = sys.argv[1]
if not audio_path.endswith('/') and not audio_path.endswith('\\'):
    audio_path = audio_path + "/"
file_name = sys.argv[2]
audio_name = file_name.split('.')[0]
result_path = sys.argv[3]
if not result_path.endswith('/') and not result_path.endswith('\\'):
    result_path = result_path + "/"

import soundfile as sf
import numpy as np
from .util_data import load_audio
sample_rate = 16000
target_vocals_path = audio_path + audio_name + "_real_vocals.wav"
target_others_path = audio_path + audio_name + "_real_accompaniment.wav"
target_vocals = load_audio(target_vocals_path, True, sr=sample_rate)
target_others = load_audio(target_others_path, True, sr=sample_rate)
sf.write(result_path + audio_name + "_real_ss.wav",
         np.vstack((target_others, target_vocals)).T, sample_rate)

from .feature_extract import pyworld_extract, mceptral
source_vocals, source_bgm = load_audio(audio_path + file_name, False, sr=sample_rate)
source_f0, source_sp, source_ap = pyworld_extract(source_vocals, sr=sample_rate)
target_f0, target_sp, target_ap = pyworld_extract(target_vocals, sr=sample_rate)
source_feat = mceptral(source_sp)
target_feat = mceptral(target_sp)

from .alignment import ctw
cca, lp, ll = ctw(source_feat, target_feat, init='dtw', verbose=True)
wp = lp[-1]

import pyworld as pw
from .old_warpath import gen_seq_acc_wp as gen_old_seq
warp_f0 = gen_old_seq(target_f0, wp)
old_audio = pw.synthesize(warp_f0, source_sp, source_ap, sample_rate, 5)
if len(old_audio) > len(source_bgm):
    old_audio = old_audio[:len(source_bgm)]
elif len(old_audio) < len(source_bgm):
    old_audio = np.append(old_audio, np.zeros(len(source_bgm) - len(old_audio)))
sf.write(result_path + audio_name + "_mcep_ss_old.wav", 
         np.vstack((source_bgm, old_audio)).T, sample_rate)

from .new_warpath import gen_seq_acc_wp as gen_new_seq
warp_sp = gen_new_seq(source_sp, wp)
warp_ap = gen_new_seq(source_ap, wp)
new_audio = pw.synthesize(target_f0, warp_sp, warp_ap, sample_rate, 5)
if len(new_audio) > len(source_bgm):
    new_audio = new_audio[:len(source_bgm)]
elif len(new_audio) < len(source_bgm):
    new_audio = np.append(new_audio, np.zeros(len(source_bgm) - len(new_audio)))
sf.write(result_path + audio_name + "_mcep_ss_new.wav",
         np.vstack((source_bgm, new_audio)).T, sample_rate)