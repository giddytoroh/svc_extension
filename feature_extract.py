"""
feature extraction
"""
import numpy as np
import pyworld as pw
import pysptk as ps
import librosa
from sklearn.preprocessing import StandardScaler

def pyworld_extract(audio, sr=16000, f0_low=74, f0_high=740, \
                    n_fft=1024, t_hop=5):
    """extract WORLD parameters"""
    audio = audio.astype(float)
    f0, t = pw.harvest(audio, sr, f0_floor=f0_low, f0_ceil=f0_high, \
                       frame_period=t_hop)
    f0 = pw.stonemask(audio, f0, t, sr)
    ap = pw.d4c(audio, f0, t, sr, fft_size=n_fft)
    sp = pw.cheaptrick(audio, f0, t, sr, f0_floor=f0_low, fft_size=n_fft)
    return f0, sp, ap

def mceptral(spec, order=24, alpha=0.35, norm=True):
    """
    generate mcep from spectrogram;
    input spec shape in [n_sample, n_dim]
    """
    mcep = np.array([ps.sp2mc(X_i, order, alpha) for X_i in spec])
    if norm:
        scaler = StandardScaler()
        mcep = scaler.fit_transform(mcep)
    return mcep

def logspec(audio, sr=16000, n_fft=1024, hop_size=80, n_band=64, logmag=True):
    """generate log-scaled, log-magnituded spectrum"""

    S = librosa.feature.melspectrogram(y=audio, sr=sr, n_fft=n_fft, \
                                       hop_length=hop_size, n_mels=n_band)
    if logmag:
        S = librosa.core.power_to_db(S)
    return S