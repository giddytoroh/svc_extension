import sys
import numpy as np
import librosa
import time

from .rcca import CCA
from .simpletimewarping import simpleDTW

def uniform_stretch(source, target, aln_len=None):
    """
    aln_len specifies the desired length of warping path wp_len;
    if not given, wp_len is determined by the longer input seq.
    NOTE: input shape assumed to be [n_sample, n_feature]
    """
    tmp = [source, target]
    max_len = max([x.shape[0] for x in tmp])
    if not aln_len:
        wp_len = max_len
    else:
        assert aln_len >= max_len, "given aln_len should be longer than both"
        wp_len = aln_len
    wp = np.zeros((wp_len, len(tmp)), int)

    for j in range(wp.shape[1]):
        wp[:, j] = np.linspace(0, tmp[j].shape[0]-1, num=wp.shape[0]).round()
    return wp

def dtw(source, target, metric='sqeuclidean'):
    """
    NOTE: input shape assumed to be [n_sample, n_feature]
    """
    wp = simpleDTW(source.T, target.T, metric=metric)[1][::-1]
    return wp

def ctw(source, target, th_lossdiff=0.0001, n_iter=20, reg=0.,
        init='uniform', verbose=False, n_cca=None, init_wp=None, **kwargs):
    """
    -Input
        source: seq1
        target: seq2
        n_cca: dimension of the embedded space
        th_lossdiff: stopping criteria
        n_iter: stopping criteria
        reg: regularization factor
        init: method for initializing warping path, in ['uniform', 'dtw']; \
              it is ignored if init_wp is given.
        init_wp: initial warping path
        NOTE: source and target are in shape of [n_sample, n_feature]
        **kwargs includes
        - metric: distance metric for DTW, default is 'sqeuclidean'
        - aln_len: the desired length of warping path for uniform stretching
    """
    assert init in ['dtw', 'uniform'], \
                "initialization method is either DTW or \
                 uniform stretching; ignored if init_wp is given"

    list_loss = []
    list_wp = []
    min_dim = min(source.shape[1], target.shape[1])
    if not n_cca:
        dim_cca = min_dim
    elif 0 < n_cca < 1:
        dim_cca = min_dim
    elif isinstance(n_cca, int) and 0 < n_cca <= min_dim:
        dim_cca = n_cca
    else:
        raise ValueError("n_cca is positive, either a float < 1 or \
                          an integer <= min_dim")
    cca = CCA(reg=reg, numCC=dim_cca, kernelcca=False, verbose=False)

    for i in range(n_iter):
        # initialize warping path
        if i == 0:
            # determine initializing method;
            # not giving init_wp:
            if not init_wp:
                # dtw-initialized
                if init == 'dtw':
                    wp = dtw(source, target, **kwargs)
                # uniform-initialized
                else:
                    wp = uniform_stretch(source, target, **kwargs)
            # giving init_wp:
            else:
                wp = init_wp

        list_wp.append(wp)
        
        """fix W, calculate V"""
        source_aln = source[wp[:, 0]]
        target_aln = target[wp[:, 1]]
        cca.train([source_aln, target_aln])
        # if n_cca is not specified as an integer
        if n_cca:
            if i == 0 and 0 < n_cca < 1:
                # re-determine dimension of the embedded space
                # accord. to the given energy to preserve (0<energy<1)
                accu_cor = 0
                for n_j, j in enumerate(cca.cancorrs):
                    accu_cor += j
                    if accu_cor >= n_cca:
                        dim_cca = n_j + 1
                        break
                cca = CCA(reg=reg, numCC=dim_cca, \
                            kernelcca=False, verbose=False)
            cca.train([source_aln, target_aln])
        # discard duplicated frames
        source_cca = cca.comps[0][np.unique(wp[:, 0], return_index=True)[1]]
        target_cca = cca.comps[1][np.unique(wp[:, 1], return_index=True)[1]]

        """fix V, calculate W"""
        wp = dtw(source_cca, target_cca, **kwargs)

        """calculate loss, and check stopping criteria"""
        loss = np.square(source_cca[wp[:, 0]] - target_cca[wp[:, 1]]).sum()
        list_loss.append(loss)

        if i == 0:
            loss_diff = loss
        else:
            loss_diff = np.abs(last_loss - loss)
        last_loss = loss

        if verbose:
            print('{}-th iteration, loss={}, loss_diff={}'.format(
                i, loss, loss_diff))
        
        if loss_diff <= th_lossdiff:
            break

    return cca, list_wp, list_loss